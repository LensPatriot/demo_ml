import sys
import numpy as np
from taipy.gui import Gui
from PIL import Image
import io
import base64

from tensorflow.keras import models

prob = 0
pred = ""


model =  models.load_model("baseline.keras")


class_names = {
    0: 'airplane',
    1: 'automobile',
    2: 'bird',
    3: 'cat',
    4: 'deer',
    5: 'dog',
    6: 'frog',
    7: 'horse',
    8: 'ship',
    9: 'truck',
}

def pred_image(model,path_to_img):
    img = Image.open(path_to_img)
    img = img.convert("RGB")
    img = img.resize((32,32))
    data = np.asarray(img)
    data = data/255
    probs = model.predict(np.array([data])[:1])
    top_probs = probs.max()
    top_pred = class_names[(np.argmax(probs))]

    return top_probs, top_pred


content=""
img_path=""

index = """
<|text-center|
<|{"logo.png"}|image|>
>
<|{content}|file_selector|extensions=.png,.jpeg,.jpg|>
Upload an image ...!

<|{pred}|>
<|{img_path}|image|>

<|{prob}|indicator|value={prob}|min=0|width=25vw|max=100|>

"""
app = Gui(page=index)

# if __name__ == "__main__":
#     reloader = '--reloader' in argv
#     app.run(port=8080, debug=True, use_reloader=reloader)

def on_change(state,ver_name,var_value):
    if ver_name == "content" :
        top_probs, top_pred = pred_image(model,var_value)
        state.prob = top_probs * 100
        state.pred = "Selected image is a " + top_pred
        state.img_path=var_value

def main(argv):
    reloader = '--reloader' in argv
    print('Starting with reloader={}'.format(reloader))
    app.run(host='0.0.0.0', port=8080, debug=True, reloader=True)

if __name__ == '__main__':
    main(sys.argv)